publish:
	dotnet publish -c Release --no-self-contained --use-current-runtime -p:PublishSingleFile=true -o publish
publish-sc:
	dotnet publish -c Release --sc --use-current-runtime -p:PublishSingleFile=true -o publish
clean:
	rm -rf publish
install:
	install -Dm755 publish/ReactGame "$(DESTDIR)/reactgame"
uninstall:
	rm $(DESTDIR)/reactgame 
