﻿using ReactGame.Core;
#pragma warning disable CS8618

namespace ReactGame;

public static class Program
{
    private static PreviewMenu _previewMenu;
    public static Game Game;
    public static Difficulty Difficulty = Difficulty.Easy;

    public static void Main()
    {
        _previewMenu = new PreviewMenu();
        Game = new Game();

        _previewMenu.Draw();
    }
}
