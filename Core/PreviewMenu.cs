﻿#pragma warning disable CS8794
#pragma warning disable CS8602
namespace ReactGame.Core;

public class PreviewMenu : Menu
{
    public override void Draw()
    {
        Console.Clear();

        AskForDifficulty();
        AskForStartGame();
    }

    private static void AskForDifficulty()
    {
        Console.Clear();
        PrintAsciiName();
        
        string[] names = Enum.GetNames(typeof(Difficulty));
        
        for (int i = 1; i <= names.Length; i++)
        {
            WriteText($"{i}. {names[i - 1]}\n", ConsoleColor.Blue);
        }

        WriteText("Select difficulty number (1-4): ");

        string? userInput = Console.ReadLine();
        bool inputIsWrong = !int.TryParse(userInput, out var inputNumber);

        if (inputIsWrong)
        {
            return;
        }

        if (inputNumber is not (1 or 2 or 3 or 4))
        {
            return;
        }

        Program.Difficulty = (Difficulty)inputNumber;
    }

    private static void AskForStartGame()
    {
        while (true)
        {
            Console.Clear();
            PrintAsciiName();
            
            WriteText("Start the game (Y/n)? ");

            string? userInput = Console.ReadLine();
            Console.Write("\n");

            if (userInput == null) AskForStartGame();

            switch (userInput.ToLower())
            {
                case "нет":
                case "не":
                case "н":
                case "n":
                case "not":
                case "no":
                    Console.Clear();
                    continue;
                case "да":
                case "д":
                case "y":
                case "yes":
                    Program.Game.Draw();
                    break;
                default:
                    Console.Clear();
                    continue;
            }

            break;
        }
    }

    private static void PrintAsciiName()
    {
        Console.ForegroundColor = ConsoleColor.Red;

        // Print the ASCII image
        Console.WriteLine(
            "______                _   _____                      \n" +
            "| ___ \\              | | |  __ \\                     \n" +
            "| |_/ /___  __ _  ___| |_| |  \\/ __ _ _ __ ___   ___ \n" +
            "|    // _ \\/ _` |/ __| __| | __ / _` | '_ ` _ \\ / _ \\ \n" +
            "| |\\ \\  __/ (_| | (__| |_| |_\\ \\ (_| | | | | | |  __/\n" +
            "\\_| \\_\\___|\\__,_|\\___|\\__|\\____/\\__,_|_| |_| |_|\\___|\n"
        );

        Console.ForegroundColor = ConsoleColor.DarkGray;
    }
}