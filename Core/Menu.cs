﻿namespace ReactGame.Core;

public abstract class Menu
{
    /// <summary>
    /// Draws the menu
    /// </summary>
    public abstract void Draw();

    /// <summary>
    /// Prints the text
    /// </summary>
    /// <param name="text">Output text</param>
    /// <param name="color">Color used</param>
    private protected static void WriteText(
        string text, ConsoleColor color = ConsoleColor.Gray)
    {
        Console.ForegroundColor = color;
        Console.Write(text);

        Console.ForegroundColor = ConsoleColor.Gray;
    }

    /// <summary>
    /// Gradually prints the text
    /// </summary>
    /// <param name="text">Output text</param>
    /// <param name="delay">Character spacing</param>
    /// <param name="color">Color used</param>
    private protected static void WritingText(
        string text, float delay, ConsoleColor color = ConsoleColor.Gray)
    {
        Console.ForegroundColor = color;

        foreach (var character in text)
        {
            Console.Write(character);
            Thread.Sleep((int) (delay * 1000));
        }

        Console.ForegroundColor = ConsoleColor.Gray;
    }
}