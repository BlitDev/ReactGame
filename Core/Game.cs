﻿using System.Diagnostics;
namespace ReactGame.Core;

public class Game : Menu
{
    private readonly Random _random;
    private int _goals;
    private readonly List<long> _goalsTime;

    public Game()
    {
        _random = new Random();
        _goalsTime = new List<long>();
    }

    /// <summary>
    /// Infinite rendering cycle
    /// </summary>
    public override void Draw()
    {
        while (true)
        {
            Console.Clear();
            DrawSection(out int targetBlock);

            if (targetBlock == -1)
            {
                WriteText("\nError when counting blocks...", ConsoleColor.Red);
                WriteText("\nExiting game...", ConsoleColor.Yellow);
                Thread.Sleep(2000);

                Environment.Exit(0);
            }

            var stopWatch = new Stopwatch();

            stopWatch.Start();
            
            string? userInput = Console.ReadLine();
            int.TryParse(userInput, out var inputNumber);
            
            if (userInput == "exit")
            {
                WriteText("\nExiting game...", ConsoleColor.Yellow);
                Thread.Sleep(1000);

                Environment.Exit(0);
            }

            stopWatch.Stop();

            if (stopWatch.ElapsedMilliseconds > 10000 / (int)Program.Difficulty)
            {
                WriteText("\nTime is over!", ConsoleColor.Red);
                Thread.Sleep(1000);
                
                ShowResults();

                Environment.Exit(0);
            }

            if (inputNumber != targetBlock)
            {
                WriteText("\nWrong answer.", ConsoleColor.Red);
                Thread.Sleep(1000);
    
                ShowResults();
                
                Environment.Exit(0);
            }

            _goals++;
            _goalsTime.Add(stopWatch.ElapsedMilliseconds);
        }
        // ReSharper disable once FunctionNeverReturns
    }

    private void ShowResults()
    {
        Console.Clear();

        if (_goalsTime.Count == 0) return;

        PrintAsciiResults();
        
        long averageGoalsTime = _goalsTime.Sum() / _goalsTime.Count;
        
        WritingText($"Your average reaction time: {averageGoalsTime} ms.\n",
            0.025f, ConsoleColor.Yellow);
        WritingText($"Total number of goals: {_goals}.\n",
            0.025f, ConsoleColor.Blue);
        
        Thread.Sleep(1500);
    }

    private void PrintAsciiResults()
    {
        Console.ForegroundColor = ConsoleColor.Red;

        // Print the ASCII image
        Console.WriteLine(
        ".______       _______     _______. __    __   __      .___________.    _______.\n" +
        "|   _  \\     |   ____|   /       ||  |  |  | |  |     |           |   /       |\n" +
        "|  |_)  |    |  |__     |   (----`|  |  |  | |  |     `---|  |----`  |   (----`\n" +
        "|      /     |   __|     \\   \\    |  |  |  | |  |         |  |        \\   \\    \n" +
        "|  |\\  \\----.|  |____.----)   |   |  `--'  | |  `----.    |  |    .----)   |   \n" +
        "| _| `._____||_______|_______/     \\______/  |_______|    |__|    |_______/    \n"
        );

        Console.ForegroundColor = ConsoleColor.DarkGray;
    }
    
    /// <summary>
    /// Draw blocks section
    /// </summary>
    /// <returns>Target block number from left to right</returns>
    private void DrawSection(out int targetBlock)
    {
        int leftCount = _random.Next(4 * (int) Program.Difficulty);
        int rightCount = _random.Next(4 * (int) Program.Difficulty);

        int rawTargetBlock = -1;

        WriteText($"Current number of goals: {_goals}.\n", ConsoleColor.Blue);
        
        for (int i = 0; i <= leftCount; i++)
        {
            if (i == leftCount)
            {
                WriteText("* ", ConsoleColor.Yellow);

                rawTargetBlock = leftCount + 1;
            }
            
            int symbol = _random.Next(2);

            switch (symbol)
            {
                case 0:
                    WriteText("= ");
                    break;
                case 1:
                    WriteText("+ ");
                    break;
                case 2:
                    WriteText("- ");
                    break;
            }
        }

        for (int i = 0; i <= rightCount; i++)
        {
            if (rightCount <= 0) break;

            int symbol = _random.Next(2);

            switch (symbol)
            {
                case 0:
                    WriteText("= ");
                    break;
                case 1:
                    WriteText("+ ");
                    break;
                case 2:
                    WriteText("- ");
                    break;
            }
        }

        Console.Write("\n\n");

        targetBlock = rawTargetBlock;
    }
}