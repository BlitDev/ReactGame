﻿namespace ReactGame.Core;

/// <summary>
/// Difficulty multiplier
/// </summary>
public enum Difficulty
{
    // ReSharper disable UnusedMember.Global
    Easy = 1,
    Normal = 2,
    Hard = 3,
    JoJoMode = 4
}