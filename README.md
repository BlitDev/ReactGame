﻿## ReactGame

#### How to play:
>The goal of the game is to name the correct sequence number of the yellow star from left to right.
The first block on the left may be a star, or it may be space debris. (Sequence number 1).
Space debris is gray, the star is yellow.
The length of the line with blocks and the waiting time varies randomly and depends on the selected difficulty.
If you choose the wrong difficulty, the easy one will be selected by default.

>**Attention: Only Jonathan Jostar can play the game on difficulties higher than Normal!**
*Explanation: At high difficulties,
> the required reaction speed and the length of the string with blocks are significantly increased.*

**IMPORTANT: Do not press the buttons during character-by-character text output. This can cause bugs.**

Build Dependencies:
- Git CLI
- .NET 6.0 SDK

#### How to build:
1. Build using Makefile (only on Linux):
>Clone repo ```git clone https://github.com/BlitGaming/ReactGame.git```
> 
>Move to folder ```cd ReactGame```
> 
>Run ```make publish``` in terminal. (The executable file will be in publish/)
> 
>Run ```sudo make install DESTDIR=/usr/bin``` to install game.
2. Build on Windows:
>Run ```dotnet publish -c Release --sc --use-current-runtime -p:PublishSingleFile=true -o publish```
in console. (The executable file will be in publish/)
3. Build from Arch Linux **AUR**:
> With **yay** AUR manager: ```yay -S reactgame```
